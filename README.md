## mavenMultiProjectSpringIntegrationHttpClient

**Spring4 + maven + spring integration **

Proyecto prueba de consumo de servicio REST con Spring Integration

Generacion de proyecto 'padre' (desde directorio de workspace)

`mvn archetype:generate`

Cambiamos el paquete (packaging) del pom.xml de jar a pom.

**server:** (desde directorio 'padre')

`mvn archetype:generate -DgroupId=org.mbracero -DartifactId=server -DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false`

**client:** (desde directorio 'padre')

`mvn archetype:generate -DgroupId=org.mbracero -DartifactId=client -DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false`

Para correr el servidor:

`mvn -pl server/ jetty:run`

CustomTest2 no funciona, aproximacion para tener spring integration con anotaciones en java.

