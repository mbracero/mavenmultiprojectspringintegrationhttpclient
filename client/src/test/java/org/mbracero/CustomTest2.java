package org.mbracero;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mbracero.config.IntegrationAnnotationConfigContextLoader;
import org.mbracero.integration.annotation.RequestGateway2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {IntegrationAnnotationConfigContextLoader.class})
//@ContextConfiguration(locations = "/test-context.xml", loader = IntegrationAnnotationConfigContextLoader.class)
public class CustomTest2 {
	
	@Autowired
	RequestGateway2 requestGateway2;

	@Test
	public void callEcho() {
		try {
			String reply = requestGateway2.echo();  
			System.out.println("Replied with: " + reply);
			assertEquals("Ok", "Ok");
		} catch (Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	/*
	@Test
	public void callGetItem() {
		try {
			Item item = requestGateway.getItem();  
			System.out.println("Replied with: " + item);
			assertEquals("Ok", "Ok");
		} catch (Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	
	@Test
	public void callGetItems() {
		try {
			List<Item> items = requestGateway.getItems();  
			System.out.println("Replied with: " + items);
			assertEquals("Ok", "Ok");
		} catch (Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	*/
}
