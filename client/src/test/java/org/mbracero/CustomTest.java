package org.mbracero;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mbracero.integration.RequestGateway;
import org.mbracero.integration.model.Request;
import org.mbracero.model.Item;
import org.mbracero.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/integration/http-outbound.xml"})
public class CustomTest {
	
	@Autowired
	RequestGateway requestGateway;
	
	@Autowired
	private RestTemplate restTemplate;

	@Test
	public void callEcho() {
		try {
			String reply = requestGateway.echo();  
			System.out.println("Replied with: " + reply);
			assertEquals("Ok", "Ok");
		} catch (Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	
	@Test
	public void callGetItem() {
		try {
			Item item = requestGateway.getItem();  
			System.out.println("Replied with: " + item);
			assertEquals("Ok", "Ok");
		} catch (Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	
	@Test
	public void callGetItems() {
		try {
			List<Item> items = requestGateway.getItems();  
			System.out.println("Replied with: " + items);
			assertEquals("Ok", "Ok");
		} catch (Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	
	@Test
	public void callGetItemById() {
		try {
			Request request = new Request();
			request.setIdSearch(2);
			request.setNameSearch("Manuel");
			request.setCitySearch("Spain");
			Item item = requestGateway.getItemById(request);  
			System.out.println("Replied with: " + item);
			assertEquals("Ok", "Ok");
		} catch (Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	
	@Test
	public void callGetPersons() {
		try {
			List<Person> persons = requestGateway.getPersons();  
			System.out.println("Replied with: " + persons);
			assertEquals("Ok", "Ok");
		} catch (Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	
	@Test
	public void callPostItemRetrievePerson() {
		try {
			Item newItem = new Item(99L, "ssss");
			
			Map<String, String> requestMap = new HashMap<String, String>();
	        requestMap.put("id", newItem.getId().toString());
	        requestMap.put("name", newItem.getName());
	        
	        
			Object person = requestGateway.postItem(newItem);  
			System.out.println("Replied with: " + person);
			assertEquals("Ok", "Ok");
		} catch (Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	
	@Test
	public void testPostItemRetrievePerson() throws Exception{
		//Map<String, Object> employeeSearchMap = getEmployeeSearchMap("0");

		final String fullUrl = "http://localhost:9091/server/api/item";
		Item body = new Item(23l,  "sdsdsd");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", "application/json");
		HttpEntity<Object> request = new HttpEntity<Object>(body, headers);

		ResponseEntity<Person> httpResponse = restTemplate.exchange(fullUrl, HttpMethod.POST, request, Person.class);
		System.out.println("Return Body :"+httpResponse.getBody());
		System.out.println("Return Status :"+httpResponse.getHeaders().get("X-Return-Status"));
		System.out.println("Return Status Message :"+httpResponse.getHeaders().get("X-Return-Status-Msg"));
		assertTrue(httpResponse.getStatusCode().equals(HttpStatus.OK));
	}
	
}
