package org.mbracero.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;

@Configuration
@EnableIntegration
@IntegrationComponentScan(basePackages={"org.mbracero.integration"})
public class IntegrationAnnotationConfigContextLoader {

}
