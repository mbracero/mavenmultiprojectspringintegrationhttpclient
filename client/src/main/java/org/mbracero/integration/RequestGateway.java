package org.mbracero.integration;

import java.util.List;
import java.util.Map;

import org.mbracero.integration.model.Request;
import org.mbracero.model.Item;
import org.mbracero.model.Person;
import org.springframework.messaging.handler.annotation.Payload;

public interface RequestGateway {
	/**
	 * Invoking No-Argument Methods
	 * http://docs.spring.io/spring-integration/reference/html/messaging-endpoints-chapter.html#gateway-calling-no-argument-methods
	 */
	@Payload("new java.lang.String()")
	String echo();
	
	@Payload("new java.lang.String()")
	Item getItem();
	
	@Payload("new java.util.ArrayList()")
	List<Item> getItems();
	
	Item getItemById(Request request);

	@Payload("new java.util.ArrayList()")
	List<Person> getPersons();
	
	
	
	//Object postItem(Map<String, String> request);

	Object postItem(Item newItem);
	
}
