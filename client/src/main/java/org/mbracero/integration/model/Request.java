package org.mbracero.integration.model;

public class Request {
	private int idSearch;
	private String nameSearch;
	private String citySearch;
	public int getIdSearch() {
		return idSearch;
	}
	public void setIdSearch(int idSearch) {
		this.idSearch = idSearch;
	}
	public String getNameSearch() {
		return nameSearch;
	}
	public void setNameSearch(String nameSearch) {
		this.nameSearch = nameSearch;
	}
	public String getCitySearch() {
		return citySearch;
	}
	public void setCitySearch(String citySearch) {
		this.citySearch = citySearch;
	}
	
	
	
	
}
