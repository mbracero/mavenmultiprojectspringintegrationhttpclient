package org.mbracero.integration;

import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandlingException;

public class ErrorHandlerMessageTransformationException {    
    @ServiceActivator
    public void handleError(Message<MessageHandlingException> message) throws Exception {
        String requestedValue = (String) message.getPayload().getFailedMessage().getPayload();
        throw message.getPayload();
    }
}