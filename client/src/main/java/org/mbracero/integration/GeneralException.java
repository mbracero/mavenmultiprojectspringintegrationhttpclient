package org.mbracero.integration;

import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandlingException;

public class GeneralException {    
    @ServiceActivator
    public void handleError(Message<Exception> message) throws Exception {
        throw message.getPayload();
    }
}