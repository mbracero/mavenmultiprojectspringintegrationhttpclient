package org.mbracero.integration.annotation;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.handler.annotation.Payload;

@MessagingGateway(name = "requestGateway2", errorChannel = "errorChannel")
public interface RequestGateway2 {

	@Gateway(requestChannel = "requestChannel", replyTimeout = 2, requestTimeout = 200)
	@Payload("new java.lang.String()")
	String echo();

}
