package org.mbracero.integration;

import org.springframework.integration.MessageTimeoutException;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;

public class ErrorHandlerMessageTimeoutException {    
    @ServiceActivator
    public void handleError(Message<MessageTimeoutException> message) throws Exception {
        String requestedValue = (String) message.getPayload().getFailedMessage().getPayload();
        throw message.getPayload();
    }
}