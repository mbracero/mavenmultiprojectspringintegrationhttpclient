package org.mbracero.controller;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.mbracero.model.Person;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PersonController {
	@SuppressWarnings("serial")
	private List<Person> persons = new CopyOnWriteArrayList<Person>() {{
		add(new Person(1l, "Person 1", "Surname 1"));
		add(new Person(2l, "Person 2", "Surname 2"));
		add(new Person(3l, "Person 3", "Surname 3"));
		add(new Person(4l, "Person 4", "Surname 4"));
		add(new Person(5l, "Person 5", "Surname 5"));
		add(new Person(6l, "Person 6", "Surname 6"));
	}};

	@RequestMapping(value="/api/persons", method=RequestMethod.GET)
	public @ResponseBody Collection<Person> getPersonsDefault() {
		System.out.println("Peticion /api/persons :: " + persons.toString());
		return persons;
	}
	
	
}